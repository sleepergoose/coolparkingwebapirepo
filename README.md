# CoolParking WebApi #

Splitting the CoolParking into client and server. Interaction via WebApi

### Visual Studio project includes: ###

* CoolParking - console application for interaction with Parking Service on the server (client) 
* CoolParking.BL - businnes logic, models, interfaces
* CoolParking.BL.Tests - tests for businnes logic
* CoolParking.WebAPI - AspNet WebApi project (server)