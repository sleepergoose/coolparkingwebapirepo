﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    // According to the task, there should be only one object of this class for the entire program. 
    // So the Parking Class was implemented according to the Singleton Pattern in the same way as in "C# in Depth" (Jon Skeet):
    // "not quite as lazy, but thread-safe without using locks"
    public sealed class Parking 
    {
        // The only Parking instance for the Singleton Pattern
        private static readonly Parking instance = new Parking();
        public decimal Balance { get; set; }
        public Dictionary<string, Vehicle> Vehicles { get; set; }
        
        public static Parking Instance 
        { 
            get
            {
                return instance;
            }
        }

        // Explicit static constructor to tell C# compiler not to mark type as beforefieldinit
        static Parking() { }

        private Parking()
        {
            Balance = Settings.InitialParkingBalance;
            Vehicles = new Dictionary<string, Vehicle>();
        }
    }
}