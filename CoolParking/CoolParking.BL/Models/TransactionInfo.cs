﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    /// <summary>
    /// The transaction records each fact of payment
    /// </summary>
    public struct TransactionInfo
    {
        [JsonProperty(Order = 2)]
        [JsonPropertyName("transactionDate")]
        public DateTime TransactionTime { get; }

        [JsonProperty(Order = 0)]
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; }

        [JsonProperty(Order = 1)]
        [JsonPropertyName("sum")]
        public decimal Sum { get; }

        public TransactionInfo(DateTime transactionTime, string vehicleId, decimal sum)
        {
            this.TransactionTime = transactionTime;
            this.VehicleId = vehicleId;
            this.Sum = sum;
        }

        public override string ToString()
        {
            return  $"Time: {TransactionTime.ToLongTimeString()} | " +
                    $"Id: {VehicleId} | " +
                    $"Sum: {Sum:C}";
        }
    }
}