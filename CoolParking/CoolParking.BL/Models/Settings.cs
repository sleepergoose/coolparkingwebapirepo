﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    /// <summary>
    /// Settings class that contains  global variables and configurations required for the application
    /// </summary>
    public static class Settings
    {
        private static decimal initialParkingBalance;
        private static int parkingCapacity;
        private static double paymentPeriod;
        private static double loggingPeriod;
        private static decimal penaltyFactor;

        public static readonly string VehicleIdPatern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";

        public static decimal InitialParkingBalance
        {
            get
            {
                return initialParkingBalance;
            }
            set
            {
                if (value >= 0)
                    initialParkingBalance = value;
                else
                    throw new System.Exception("Value must be greater than zero");
            }
        }

        public static int ParkingCapacity
        {
            get
            {
                return parkingCapacity;
            }
            set
            {
                if (value >= 0)
                    parkingCapacity = value;
                else
                    throw new System.Exception("Value must be greater than zero");
            }
        }

        public static double PaymentPeriod
        {
            get
            {
                return paymentPeriod;
            }
            set
            {
                if (value >= 0 && value <= int.MaxValue)
                    paymentPeriod = value;
                else
                    throw new System.Exception($"Value must be between zero and {int.MaxValue}");
            }
        }

        public static double LoggingPeriod
        {
            get
            {
                return loggingPeriod;
            }
            set
            {
                if (value >= 0 && value <= int.MaxValue)
                    loggingPeriod = value;
                else
                    throw new System.Exception($"Value must be between zero and {int.MaxValue}");
            }
        }

        public static decimal PenaltyFactor
        {
            get
            {
                return penaltyFactor;
            }
            set
            {
                if (value >= 0)
                    penaltyFactor = value;
                else
                    throw new System.Exception("Value must be greater than zero");
            }
        }

        public static Dictionary<VehicleType, decimal> Tariffs { get; }

        static Settings()
        {
            initialParkingBalance = 0;
            parkingCapacity = 10;
            paymentPeriod = 5; // s
            loggingPeriod = 60; // s
            penaltyFactor = 2.5M;

            Tariffs = new Dictionary<VehicleType, decimal>
            {
                [VehicleType.PassengerCar] = 2m,
                [VehicleType.Truck] = 5m,
                [VehicleType.Bus] = 3.5m,
                [VehicleType.Motorcycle] = 1m
            };
        }
    }
}