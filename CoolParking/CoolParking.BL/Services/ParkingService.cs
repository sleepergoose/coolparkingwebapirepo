﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private bool disposedValue;

        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        private Parking _parking = Parking.Instance;

        private List<TransactionInfo> TransactionList;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this._withdrawTimer = withdrawTimer;
            this._logTimer = logTimer;
            this._logService = logService;

            // Subscribe to timers events 
            _withdrawTimer.Elapsed += RegularWithdraw;
            _logTimer.Elapsed += WriteLog;

            // Set timer interval
            _withdrawTimer.Interval = Settings.PaymentPeriod * 1000;
            _logTimer.Interval = Settings.LoggingPeriod * 1000;

            // Start timer services
            _withdrawTimer.Start();
            _logTimer.Start();

            TransactionList = new List<TransactionInfo>();
        }

        /// <summary>
        /// Adds a vehicle to Parking
        /// </summary>
        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle == null)
            {
                throw new ArgumentNullException($"Argument 'vehicle' is null. Argument cannot be null.");
            }
            else if (_parking.Vehicles.TryGetValue(vehicle.Id, out _) == true)
            {
                throw new ArgumentException($"Such a vehicle with Id {vehicle.Id} already exists!");
            }
            else if (_parking.Vehicles.Count >= Settings.ParkingCapacity)
            {
                throw new InvalidOperationException($"Parking is full. Can't add a new vehicle");
            }
            else
            {
                _parking.Vehicles.Add(vehicle.Id, vehicle);
            }
        }

        /// <summary>
        /// Gets Parking Balance
        /// </summary>
        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        /// <summary>
        /// Gets number of occupied places 
        /// </summary>
        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        /// <summary>
        /// Gets number of free places 
        /// </summary>
        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - _parking.Vehicles.Count;
        }

        /// <summary>
        /// Gets last transactions
        /// </summary>
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return TransactionList.ToArray();
        }

        /// <summary>
        /// Gets a collection of the existings vehicles
        /// </summary>
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles.Select(p => p.Value).ToList());
        }

        /// <summary>
        /// Gets transaction history 
        /// </summary>
        public string ReadFromLog()
        {
            return _logService.Read();
        }

        /// <summary>
        /// Removes a vehicle by its Id
        /// </summary>
        public void RemoveVehicle(string vehicleId)
        {
            // Check the format of the given argument 'vehicleId' ("XX-YYYY-XX")
            Regex regex = new Regex(Settings.VehicleIdPatern);
            if (regex.IsMatch(vehicleId) == false)
            {
                throw new ArgumentException($"Argument 'vehicleId' has a wrong format");
            }
            else
            {
                // Check the presence of a vehicle with the given Id
                Vehicle tempVehicle;
                bool vehicleExists = _parking.Vehicles.TryGetValue(vehicleId, out tempVehicle);
                if (vehicleExists == false)
                {
                    throw new ArgumentException($"A vehicle with Id {vehicleId} doesn't exist in the Parking");
                }
                else if (tempVehicle.Balance < 0)
                {
                    throw new InvalidOperationException($"Cannot remove a vehicle with a negative balance");
                }
                else
                {
                    _parking.Vehicles.Remove(vehicleId);
                }
            }
        }

        /// <summary>
        /// Tops up a vehicle balance on a given sum
        /// </summary>
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            // Check the format of the given argument 'vehicleId' ("XX-YYYY-XX")
            Regex regex = new Regex(Settings.VehicleIdPatern);
            if (regex.IsMatch(vehicleId) == false)
            {
                throw new ArgumentException($"Argument 'vehicleId' has a wrong format");
            }
            else
            {
                // Check the presence of a vehicle with the given Id
                Vehicle tempVehicle;
                bool vehicleExists = _parking.Vehicles.TryGetValue(vehicleId, out tempVehicle);
                if (vehicleExists == false)
                {
                    throw new ArgumentException($"A vehicle with Id {vehicleId} doesn't exist in the Parking");
                }
                else if (sum < 0)
                {
                    throw new ArgumentException($"Cannot topup a vehicle on the negative sum");
                }
                else
                {
                    _parking.Vehicles[vehicleId].Balance += sum;
                }
            }
        }

        /// <summary>
        /// Writes to log
        /// </summary>
        public void WriteLog(object sender, ElapsedEventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (TransactionInfo info in TransactionList.ToArray())
            {
                stringBuilder.AppendLine(info.ToString());
            }
            _logService.Write(stringBuilder.ToString());
            TransactionList.Clear();
        }

        /// <summary>
        /// Withdraws money from vehicle balance
        /// </summary>
        public void RegularWithdraw(object sender, ElapsedEventArgs e)
        {
            var vehicles = GetVehicles();
            if (vehicles != null)
            {
                foreach (Vehicle vehicle in vehicles)
                {
                    decimal tariff = Settings.Tariffs[vehicle.VehicleType];
                    decimal withdrawing = 0;
                    if (vehicle.Balance <= 0)
                    {
                        withdrawing = Settings.PenaltyFactor * tariff;
                        _parking.Vehicles[vehicle.Id].Balance -= withdrawing;
                        _parking.Balance += withdrawing;
                    }
                    else if (vehicle.Balance - tariff < 0)
                    {
                        withdrawing = (vehicle.Balance + Settings.PenaltyFactor * (tariff - vehicle.Balance));
                        _parking.Vehicles[vehicle.Id].Balance -= withdrawing;
                        _parking.Balance += withdrawing;
                    }
                    else
                    {
                        withdrawing = tariff;
                        _parking.Vehicles[vehicle.Id].Balance -= withdrawing;
                        _parking.Balance += withdrawing;
                    }
                    TransactionList.Add(new TransactionInfo(transactionTime: DateTime.Now, vehicleId: vehicle.Id, sum: withdrawing));
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if(string.IsNullOrEmpty(_logService.LogPath) == false)
                    {
                        File.Delete(_logService.LogPath);
                    }
                    _logTimer.Dispose();
                    _withdrawTimer.Dispose();
                }
                // Set large fields to null
                _parking.Balance = Settings.InitialParkingBalance;
                _parking.Vehicles.Clear();
                TransactionList = null;

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            System.GC.SuppressFinalize(this);
        }
    }
}