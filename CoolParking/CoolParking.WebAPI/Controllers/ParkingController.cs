﻿using System;
using System.Linq;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CoolParking.WebAPI.RequestModels;
using System.Net.Mime;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parking = null;

        public ParkingController(IParkingService parking)
        {
            _parking = parking;
        }

        /* Gets parking balance */
        [HttpGet] 
        [Route("api/parking/balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parking.GetBalance());
        }

        /* Gets parking capacity */
        [HttpGet]
        [Route("api/parking/capacity")]
        public ActionResult<int> GetCpacity()
        {
            return Ok(_parking.GetCapacity());
        }

        /* Gets free places in the parking */
        [HttpGet]
        [Route("api/parking/freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parking.GetFreePlaces());
        }

        /* Gets a list of vehicles in the parking lot */
        [HttpGet]
        [Route("api/vehicles")]
        public ActionResult<ReadOnlyCollection<Vehicle>> GetVehicles()
        {
            return Ok(_parking.GetVehicles());
        }

        /* Gets a vehicle by its Id */
        [HttpGet]
        [Route("api/vehicles/{id}")] //(id - vehicle id of format "AA-0001-AA")
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            Regex regex = new Regex(Settings.VehicleIdPatern);
            if (string.IsNullOrEmpty(id) == false && regex.IsMatch(id) == true)
            {
                Vehicle vehicle = _parking.GetVehicles().Where(v => v.Id == id).FirstOrDefault();
                if (vehicle != null)
                    return Ok(vehicle);
                else
                    return NotFound($"There is no a vehicle with Id: {id}");
            }
            else
            {
                return BadRequest("Entered Id is invalid!");
            }
        }

        /* POST: Adds a vehicle in the parking */
        [HttpPost]
        [Route("api/vehicles")]
        public ActionResult<Vehicle> AddVehicle(VehicleRequest vehicle)
        {
            if (vehicle != null)
            {
                try
                {
                    Regex regex = new Regex(Settings.VehicleIdPatern);
                    int[] values = new[] { 0, 1, 2, 3 };

                    if (regex.IsMatch(vehicle.Id) == false || vehicle.Balance < 0 || values.Contains((int)vehicle.VehicleType) == false)
                        return BadRequest("Bad request! Entered data were wrong.");
                    else
                    {
                        _parking.AddVehicle(new Vehicle(id: vehicle.Id, vehicleType: vehicle.VehicleType, balance: vehicle.Balance));
                        return Created($@"api/vehicles/{vehicle.Id}", vehicle);
                    }
                }
                catch (ArgumentException ex)
                {
                    return BadRequest("Bad request! " + ex.Message);
                }
            }
            else
            {
                return BadRequest("Bad request! Given data is invalid or equal to null");
            }
        }

        /* DELETE: Removes a vehicle by its Id */
        [HttpDelete]
        [Route("api/vehicles/{id}")] // vehicle id of format "AA-0001-AA"
        public ActionResult DeleteVehicle(string id)
        {
            Regex regex = new Regex(Settings.VehicleIdPatern);
            if (string.IsNullOrEmpty(id) == false && regex.IsMatch(id) == true)
            {
                try
                {
                    _parking.RemoveVehicle(id);
                    return NoContent();
                }
                catch (ArgumentException)
                {
                    return NotFound($"Such a vehicle with Id {id} does not exist!");
                }
                catch (InvalidOperationException ex)
                {
                    return BadRequest("Bad request! " + ex.Message);
                }
            }
            else
            {
                return BadRequest("Entered Id is invalid!");
            }
        }

        /* GET: Gets a list of current transactions */
        [HttpGet]
        [Route("api/transactions/last")]
        public ActionResult<TransactionInfo[]> GetLastTransaction()
        {
            return Ok(_parking.GetLastParkingTransactions());
        }

        /* GET: Gets history of all the transactions from log */
        [HttpGet]
        [Route("api/transactions/all")]
        public ActionResult<string> GetAllTransaction()
        {
            try
            {
                return Ok(_parking.ReadFromLog());
            }
            catch (InvalidOperationException)
            {
                return NotFound("Transaction history is empty!");
            }
            catch (Exception ex)
            {
                return BadRequest("Bad request! " + ex.Message);
            }
        }


        /* GET: Updates a vehicle balance by Id */
        [HttpPut]
        [Route("api/transactions/topUpVehicle/")]
        public ActionResult TopUpVehicle(TopUp topup)
        {
            Regex regex = new Regex(Settings.VehicleIdPatern);
            try
            {
                if (topup == null)
                {
                    return BadRequest("Given parameters is not valid");
                }
                else if(topup.Sum < 0)
                {
                    return BadRequest("Bad Request! Sum must be above zero.");
                }
                else if (regex.IsMatch(topup.Id) == false)
                {
                    return BadRequest("Bad Request! Given wrong Id.");
                }
                else
                {
                    _parking.TopUpVehicle(topup.Id, topup.Sum);
                    Vehicle vehicle = _parking.GetVehicles().Where(v => v.Id == topup.Id).FirstOrDefault();
                    VehicleRequest vehicleRequest = new VehicleRequest() { Id = vehicle.Id, Balance = vehicle.Balance, VehicleType = vehicle.VehicleType };
                    return Ok(System.Text.Json.JsonSerializer.Serialize<VehicleRequest>(vehicleRequest));
                }
            }
            catch (ArgumentException)
            {
                return NotFound("There is no sach a vehicle!");
            }

        }
    }
}
