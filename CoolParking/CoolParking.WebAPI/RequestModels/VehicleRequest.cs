﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.RequestModels
{
    public class VehicleRequest
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }
    }
}
