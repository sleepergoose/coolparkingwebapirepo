﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.RequestModels
{
    public class TopUp
    {
        // [JsonProperty("id", Order = 0)]
        [JsonPropertyName("id")]
        public string Id { get; set; }

        // [JsonProperty("Sum", Order = 1)]
        [JsonPropertyName("Sum")]
        public decimal Sum { get; set; }
    }
}
