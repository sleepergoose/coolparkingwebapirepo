using System.IO;
using System.Reflection;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        // Path to LogFIle
        private static readonly string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc(options => options.EnableEndpointRouting = false);
            // services.AddSwaggerGen();

            // Parking Service start
            services.AddTransient<ITimerService, TimerService>();
            services.AddTransient<ILogService, LogService>(serviceProvider => {
                return new LogService(logFilePath);
            });
            // another way to run LogService with parameters:
            // services.AddSingleton<ILogService, LogService>(_ => new LogService(logFilePath));

            services.AddSingleton<IParkingService, ParkingService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            //});
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
