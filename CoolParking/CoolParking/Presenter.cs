﻿using System;
using System.Collections.Generic;
using CoolParking.Interfaces;
using System.Linq;
using System.IO;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.RequestModels;
using System.Text.RegularExpressions;
using System.Net;

namespace CoolParking
{
    public class Presenter
    {
        private HttpClient _client = new HttpClient();
        private List<string> _options = new List<string>()
        {
            "Current parking balance",
            "Parking capacity",
            "Number of free/occupied parking spaces",
            "Transactions for the current period",
            "Transactions history",
            "List of vehicles in the Parking lot",
            "Put the vehicle on the Parking lot",
            "Pick up a vehicle from the Parking lot",
            "Top up a balance of a vehicle",
            "Get vehicle by Id",
            "Set Host Address",
            "Quit"
        };
        private readonly IView _view = null;

        private string _hostAddress = @"https://localhost:44350/";

        delegate void Manager(string response = "");
        Manager _optionsManager;


        public Presenter(View view)
        {
            _view = view;
            _optionsManager = OptionsManager;
        }

        // Starts manager
        public void Run()
        {
            _view.ShowOptions(_options);
            _optionsManager(_view.GetTextResponse());
        }

        /// <summary>
        /// Options manager. It's in charge of the Options menu 
        /// </summary>
        /// <param name="command"></param>
        void OptionsManager(string command)
        {
            try
            {
                switch (command)
                {
                    case "1": /* Current parking balance */
                        _view.ShowCurrentParkingBalance(GetBalanceAsync().Result);
                        break;
                    case "2": /* Income in the current period */
                        _view.ShowParkingCapacity(GetCapacityAsync().Result);
                        break;
                    case "3": /* Number of free/occupied parking spaces */
                        int freePlaces = GetFreePlacesAsync().Result;
                        _view.ShowNumberParkingSpaces(freePlaces, GetCapacityAsync().Result - freePlaces);
                        break;
                    case "4": /* ShowCurrentTransactions */
                        _view.ShowCurrentTransactions(GetTransactionsAsync().Result);
                        break;
                    case "5": /* Transactions history */
                        _view.ShowTransactionsHistory(GetHistoryLogAsync().Result);
                        break;
                    case "6": /* List of vehicles in the Parking lot */
                        _view.ShowVehiclesList(GetVehiclesAsync().Result);
                        break;
                    case "7": /* Put the vehicle on the Parking lot */
                        PutVehicle();
                        break;
                    case "8": /* Pick up a vehicle from the Parking lot */
                        PickUpVehicle();
                        break;
                    case "9": /* Top up the balance of a vehicle */
                        TopUpVehicle();
                        break;
                    case "10": /* Get vehicle by Id */
                        GetVehicleById();
                        break;
                    case "11": /* Set Host Address */
                        SetHostAddress();
                        break;
                    case "12": /* Quit */
                        AppQuit();
                        return;
                    case "clear": /* Clear screen (show main header) */
                        _view.Clear();
                        break;
                    default: /* If entered wrong command */
                        _view.WriteErrorToCenter("Entered wrong command! Try again.");
                        break;
                }
                _optionsManager(_view.GetTextResponse());
            }
            catch(AggregateException ex)
            {
                _view.WriteErrorToCenter(ex.Message);
                _view.WriteTextToCenter("Possible causes:");
                _view.WriteTextToCenter("Server is not running");
                _view.WriteTextToCenter("Host address is wrong");
                _view.WriteTextToCenter("Failed connection");
            }
            catch (Exception ex)
            {
                _view.WriteErrorToCenter("Unhandled error!");
                _view.WriteErrorToCenter(ex.Message);
                AppQuit();
            }
        }

        private async void GetVehicleById()
        {
            string id = _view.ShowGetVehicleById();
            if (id == null)
            {
                _view.WriteErrorToCenter($"Entered invalid Id!");
            }
            else
            {
                try
                {
                    Vehicle vehicle = await GetVehicleByIdAsync(id);
                    _view.ShowVehiclesList(new ReadOnlyCollection<Vehicle>(new List<Vehicle> { vehicle }));
                }
                catch (HttpRequestException ex)
                {
                    if (ex.StatusCode == HttpStatusCode.NotFound)
                        _view.WriteTextToCenter($"There is no a vehicle with such an Id! " + ex.Message);
                    else
                        _view.WriteErrorToCenter(ex.Message);
                }
            }
        }


        /// <summary>
        /// Shows screen to add a vehicle to the Parking
        /// </summary>
        private async void PutVehicle()
        {
            try
            {
                Vehicle vehicle = _view.ShowPutVehicle();
                if (vehicle != null)
                {
                    HttpStatusCode result = await PutVehicleAsync(vehicle);
                    if (result == HttpStatusCode.Created)
                    {
                        _view.WriteTextToCenter($"The {vehicle.VehicleType} {vehicle.Id} was added");
                    }
                    else
                    {
                        _view.WriteErrorToCenter("Error occured! Bad request. Operation was not complete.");
                    }
                }
                else
                {
                    _view.WriteErrorToCenter($"You entered wrond data. Please, try again.");
                }
            }
            catch (Exception)
            {
                _view.WriteErrorToCenter("Something was wrong! Operation aborted");
            }
        }


        /// <summary>
        /// Shows screen to top up money to a vehicle balance
        /// </summary>
        private async void TopUpVehicle()
        {
            try
            {
                ReadOnlyCollection<Vehicle> vehicles = GetVehiclesAsync().Result;
                IEnumerable<(string id, decimal sum)> list = _view.ShowTopUpVehicleBalance(vehicles);

                if (list != null && list.Count() != 0)
                {
                    foreach (var item in list)
                    {
                        HttpStatusCode httpStatusCode = await TopUpAsync(item.id, item.sum);

                        if (httpStatusCode == HttpStatusCode.OK)
                            _view.WriteTextToCenter($"Vehicle {item.id} toped up successfully on {item.sum:C}!");
                        else if (httpStatusCode == HttpStatusCode.NotFound)
                            _view.WriteTextToCenter($"Vehicle {item.id} was not found!");
                        else
                            _view.WriteErrorToCenter($"Vehicle {item.id} did not top up! Bad request.");
                    }
                }
                else
                {
                    _view.WriteTextToCenter($"None vehicle was toped up. Please, try again.");
                }
            }
            catch (Exception ex)
            {
                _view.WriteErrorToCenter(ex.Message);
            }

        }


        /// <summary>
        /// Shows screen to pick up a vehicle from the Parking
        /// </summary>
        private async void PickUpVehicle()
        {
            try
            {
                ReadOnlyCollection<Vehicle> vehicles = GetVehiclesAsync().Result;
                string[] vehicleIds = _view.ShowPickUpVehicle(vehicles);
                if (vehicleIds != null && vehicleIds.Length != 0)
                {
                    foreach (string id in vehicleIds)
                    {
                        if (string.IsNullOrEmpty(id) == false)
                        {
                            HttpStatusCode httpStatusCode = await DeleteAsync(id);

                            if (httpStatusCode == HttpStatusCode.NoContent)
                                _view.WriteTextToCenter($"Vehicle {id} picked up successfully!");
                            else if (httpStatusCode == HttpStatusCode.NotFound)
                                _view.WriteTextToCenter($"Vehicle {id} was not found to pick up!");
                            else
                                _view.WriteErrorToCenter($"Vehicle {id} did not pick up! Can't pick up a vehicle with a negative balance.");
                        }
                    }
                }
                else
                {
                    _view.WriteTextToCenter($"None vehicle was selected (or parking is empty). Please, try again.");
                }
            }
            catch (HttpRequestException ex)
            {
                _view.WriteErrorToCenter(ex.Message);
            }
            catch (ArgumentException ex)
            {
                _view.WriteErrorToCenter(ex.Message);
            }
            catch (Exception)
            {
                _view.WriteErrorToCenter($"None vehicle was removed (or parking is empty). Please, try again.");
            }
        }


        /// <summary>
        /// Changes host address
        /// </summary>
        private void SetHostAddress()
        {
            _view.WriteTextToCenter($"Current host address: {_hostAddress}");
            _view.WriteTextToCenter($"Enter new below");

            Regex regex = new Regex(@"^https:\/\/[a-zA-Z0-9]*:[0-9]*\/$");
            string tempHostAddress = _view.GetTextResponse();

            if (regex.IsMatch(tempHostAddress) == true)
            {
                _hostAddress = tempHostAddress;
                _view.WriteTextToCenter($"New host address accepted: {tempHostAddress}");
            }
            else
            {
                _view.WriteTextToCenter($"You entered wrong host address format: {tempHostAddress}");
            }
        }


        /// <summary>
        /// Exits from application
        /// </summary>
        private void AppQuit()
        {
            _view.WriteTextToCenter("Good Luck!");
            _view.WriteTextToCenter("...Press 'Enter' key to exit...");
            _view.GetTextResponse();
        }


        #region Requests Methods
        // Get balance
        private async Task<decimal> GetBalanceAsync()
        {
            var request = await _client.GetStringAsync(Path.Combine(_hostAddress, @$"api/parking/balance"));
            return JsonConvert.DeserializeObject<decimal>(request);
        }

        // Get capacity
        private async Task<int> GetCapacityAsync()
        {
            var request = await _client.GetStringAsync(Path.Combine(_hostAddress, @$"api/parking/capacity"));
            return JsonConvert.DeserializeObject<int>(request);
        }

        // Get free places
        private async Task<int> GetFreePlacesAsync()
        {
            var request = await _client.GetStringAsync(Path.Combine(_hostAddress, @$"api/parking/freePlaces"));
            return JsonConvert.DeserializeObject<int>(request);
        }

        // Get last transactions
        private async Task<TransactionInfo[]> GetTransactionsAsync()
        {
            var request = await _client.GetStringAsync(Path.Combine(_hostAddress, @$"api/transactions/last"));
            return JsonConvert.DeserializeObject<TransactionInfo[]>(request);
        }

        // Get last transactions
        private async Task<string> GetHistoryLogAsync()
        {
            try
            {
                var request = await _client.GetStringAsync(Path.Combine(_hostAddress, @$"api/transactions/all"));
                return JsonConvert.DeserializeObject<string>(request);
            }
            catch (HttpRequestException ex)
            {
                if (ex.StatusCode == HttpStatusCode.NotFound)
                    _view.WriteTextToCenter(ex.Message);
                else
                    _view.WriteErrorToCenter(ex.Message);
                return null;
            }
        }

        // Get vehicles
        private async Task<ReadOnlyCollection<Vehicle>> GetVehiclesAsync()
        {
            var request = await _client.GetStringAsync(Path.Combine(_hostAddress, @$"api/vehicles"));
            return JsonConvert.DeserializeObject<ReadOnlyCollection<Vehicle>>(request);
        }

        // Get vehicle by its Id
        private async Task<Vehicle> GetVehicleByIdAsync(string id)
        {
            var request = await _client.GetStringAsync(Path.Combine(_hostAddress, @$"api/vehicles/{id}"));
            return JsonConvert.DeserializeObject<Vehicle>(request);
        }

        // Put a vehicle
        private async Task<HttpStatusCode> PutVehicleAsync(Vehicle vehicle)
        {
            var content = System.Net.Http.Json.JsonContent.Create<Vehicle>(vehicle);
            var request = await _client.PostAsync(Path.Combine(_hostAddress, @$"api/vehicles"), content);
            return request.StatusCode;
        }

        // Delete a vehicle by Id
        private async Task<HttpStatusCode> DeleteAsync(string id)
        {
            var request = await _client.DeleteAsync(Path.Combine(_hostAddress, @$"api/vehicles/{id}"));
            return request.StatusCode;
        }

        // Update vehicle
        private async Task<HttpStatusCode> TopUpAsync(string id, decimal sum)
        {
            TopUp topup = new TopUp() { Id = id, Sum = sum };
            var content = System.Net.Http.Json.JsonContent.Create<TopUp>(topup);
            var request = await _client.PutAsync(Path.Combine(_hostAddress, @$"api/transactions/topUpVehicle/"), content);
            return request.StatusCode;
        }
        #endregion
    }
}
