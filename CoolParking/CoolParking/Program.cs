﻿namespace CoolParking
{
    class Program
    {
        static void Main(string[] args)
        {
            // Realizes dependency inversion
            View view = new View();

            Presenter presenter = new Presenter(view: view);
            presenter.Run();
        }
    }
}
