﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoolParking.RequestModels;

namespace CoolParking.Interfaces
{
    // Contract to ensure interaction between the presenter and the console interface
    interface IView
    {
        void Clear();
        void ShowMain();
        void ShowOptions(List<string> options);
        void ShowCurrentParkingBalance(decimal balance);
        void ShowParkingCapacity(decimal income);
        void ShowNumberParkingSpaces(int freePlaces, int occupiedPlaces);
        void ShowCurrentTransactions(TransactionInfo[] transactionInfo);
        void ShowTransactionsHistory(string history);
        void ShowVehiclesList(ReadOnlyCollection<Vehicle> vehicles);
        Vehicle ShowPutVehicle();
        string[] ShowPickUpVehicle(ReadOnlyCollection<Vehicle> vehicles);
        IEnumerable<(string id, decimal sum)> ShowTopUpVehicleBalance(ReadOnlyCollection<Vehicle> vehicles);
        void WriteTextToCenter(string text);
        void WriteErrorToCenter(string text);
        string ShowGetVehicleById();
        string GetTextResponse();
    }
}