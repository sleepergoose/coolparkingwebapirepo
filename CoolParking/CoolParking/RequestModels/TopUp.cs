﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace CoolParking.RequestModels
{
    public class TopUp
    {
        [JsonProperty("id", Order = 0)]
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonProperty("Sum", Order = 1)]
        [JsonPropertyName("Sum")]
        public decimal Sum { get; set; }
    }
}
