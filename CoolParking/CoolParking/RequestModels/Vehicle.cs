﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.RequestModels
{
    public class Vehicle
    {
        [JsonProperty("id", Order = 0)]
        public string Id { get; set; }

        [JsonProperty("vehicleType", Order = 1)]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance", Order = 2)]
        public decimal Balance { get; set; }

        public static readonly string VehicleIdPatern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";

        public static string GenerateRandomRegistrationPlateNumber()
        {
            try
            {
                Regex regex = new Regex(Vehicle.VehicleIdPatern);
                var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                StringBuilder stringBuilder = new StringBuilder();
                Random random = new Random();
                do
                {
                    // Generates the first two characters
                    stringBuilder.Append(string.Concat(Enumerable.Range(0, 2).Select(p => letters[random.Next(0, letters.Length)])));
                    stringBuilder.Append('-');
                    // Generates four digit symbols 
                    stringBuilder.Append(random.Next(1, 9999).ToString().PadLeft(4, '0'));
                    stringBuilder.Append('-');
                    // Generates the last two characters
                    stringBuilder.Append(string.Concat(Enumerable.Range(0, 2).Select(p => letters[random.Next(0, letters.Length)])));

                } while (regex.IsMatch(stringBuilder.ToString()) == false);
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
