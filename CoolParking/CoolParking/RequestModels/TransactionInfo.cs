﻿using Newtonsoft.Json;
using System;

namespace CoolParking.RequestModels
{
    public struct TransactionInfo
    {
        [JsonProperty("transactionDate", Order = 2)]
        public DateTime TransactionTime { get; set; }

        [JsonProperty("vehicleId", Order = 0)]
        public string VehicleId { get; set; }

        [JsonProperty("sum", Order = 1)]
        public decimal Sum { get; set; }
    }
}
