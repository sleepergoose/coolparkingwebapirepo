﻿using System;
using CoolParking.RequestModels;
using CoolParking.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;

namespace CoolParking
{
    public class View : IView
    {
        private static List<string> _options;

        /// <summary>
        /// Shows main screen
        /// </summary>
        /// <param name="menu">List of the main menu item</param>
        public void ShowMain()
        {
            Console.Clear();

            // Required variables
            int w = Console.WindowWidth;
            string title = "CoolParking App.";
            char symbol = '=';

            // Write titles
            Console.Title = title;
            int left = (w - title.Length) / 2;
            Console.SetCursorPosition(left, 1);
            ColorWrite($"{title}", ConsoleColor.Green);
            Console.ForegroundColor = ConsoleColor.Green;
            System.Console.WriteLine("");
            Console.WriteLine(string.Concat(Enumerable.Repeat(symbol, w - 1)));
            Console.ForegroundColor = ConsoleColor.White;
        }


        /// <summary>
        /// Fills in the text as Console.Write() with definit color
        /// </summary>
        private void ColorWrite(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ForegroundColor = ConsoleColor.White;
        }


        /// <summary>
        /// Shows main screen with options
        /// </summary>
        /// <param name="options">List of the options</param>
        public void ShowOptions(List<string> options)
        {
            Console.Clear();

            // Save options for reloading
            _options = options;

            // Load main menu
            ShowMain();
            Console.WriteLine("");

            // Required variables
            int w = Console.WindowWidth;
            char symbol = '-';
            int itemCount = options.Count();
            int leftColumn = (int)Math.Ceiling(itemCount / 2d);
            int rightColumn = itemCount - leftColumn;
            int maxItemLength = options.Where((p, i) => i < leftColumn).Max(x => x.Length);
            string title = "Options";

            // Write Options section
            Console.ForegroundColor = ConsoleColor.White;
            string line = $"{title}" + string.Concat(Enumerable.Repeat(symbol, w - title.Length - 1));
            Console.WriteLine(line);

            for (int i = 0; i < leftColumn; i++)
            {
                line = String.Format("\t{0,-1}. {1, -45} \t {2, -1} {3, -40}", (i + 1), options[i], (i + leftColumn >= itemCount ? "" : (i + leftColumn + 1) + "."), (i + leftColumn >= itemCount ? "" : options[i + leftColumn]));
                Console.WriteLine(line);
            }
            Console.WriteLine(string.Concat(Enumerable.Repeat(symbol, w - 1)));
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("Type 'clear' to clean the screen of type number of the desired option");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
        }


        /// <summary>
        /// Clean up the screen and shows Options
        /// </summary>
        public void Clear()
        {
            ShowOptions(_options);
        }


        /// <summary>
        /// Waits and gets any response
        /// </summary>
        /// <returns>Any typed response from view</returns>
        public string GetTextResponse()
        {
            (int _, int top) = Console.GetCursorPosition();
            Console.SetCursorPosition(1, top);
            return Console.ReadLine();
        }


        /// <summary>
        /// Writes yellow text to the screen. 
        /// </summary>
        public void WriteTextToCenter(string text)
        {
            // Some required variables
            int maxLineWidth = Console.WindowWidth - 15;
            List<string> lines = new List<string>();

            if (text.Length >= maxLineWidth)
            {
                // Split 'text' to lines less than maxLineWidth
                int n = text.Length / maxLineWidth;
                for (int i = 0; i < n; i++)
                {
                    lines.Add(text.Substring(i * maxLineWidth, maxLineWidth));
                }
                lines.Add(text.Substring(n * maxLineWidth, text.Length - n * maxLineWidth));

                // Print lines
                foreach (string line in lines)
                {
                    // Set cursor
                    int left = (Console.WindowWidth - line.Length) / 2;
                    Console.WriteLine("");
                    (_, int top) = Console.GetCursorPosition();
                    Console.SetCursorPosition(left, top);

                    // Draw
                    ColorWrite($"{line}", ConsoleColor.Yellow);
                    Console.WriteLine("");
                }
            }
            else
            {
                // Set cursor
                int left = (Console.WindowWidth - text.Length) / 2;
                Console.WriteLine("");
                (_, int top) = Console.GetCursorPosition();
                Console.SetCursorPosition(left, top);

                // Draw
                ColorWrite($"{text}", ConsoleColor.Yellow);
                Console.WriteLine("");
            }
        }


        /// <summary>
        /// Writes red text to the screen. 
        /// </summary>
        public void WriteErrorToCenter(string text)
        {
            // Some required variables
            int maxLineWidth = Console.WindowWidth - 15;
            List<string> lines = new List<string>();

            if (text.Length >= maxLineWidth)
            {
                // Split 'text' to lines less than maxLineWidth
                int n = text.Length / maxLineWidth;
                for (int i = 0; i < n; i++)
                {
                    lines.Add(text.Substring(i * maxLineWidth, maxLineWidth));
                }
                lines.Add(text.Substring(n * maxLineWidth, text.Length - n * maxLineWidth));

                // Print lines
                foreach (string line in lines)
                {
                    // Set cursor
                    int left = (Console.WindowWidth - line.Length) / 2;
                    Console.WriteLine("");
                    (_, int top) = Console.GetCursorPosition();
                    Console.SetCursorPosition(left, top);

                    // Draw
                    ColorWrite($"{line}", ConsoleColor.Red);
                    Console.WriteLine("");
                }
            }
            else
            {
                // Set cursor
                int left = (Console.WindowWidth - text.Length) / 2;
                Console.WriteLine("");
                (_, int top) = Console.GetCursorPosition();
                Console.SetCursorPosition(left, top);

                // Draw
                ColorWrite($"{text}", ConsoleColor.Red);
                Console.WriteLine("");
            }
        }


        /// <summary>
        /// Shows current balance of the Parking Servise
        /// </summary>
        public void ShowCurrentParkingBalance(decimal balance)
        {
            // Redraw screen
            ShowOptions(_options);
            WriteTextToCenter($"Current Parking Balance: {balance:C}");
        }


        /// <summary>
        /// Show income for the current period
        /// </summary>
        public void ShowParkingCapacity(decimal income)
        {
            // Redraw screen
            ShowOptions(_options);
            WriteTextToCenter($"Parking capacity: {income}");
        }


        /// <summary>
        /// Shows free/occupied parking places
        /// </summary>
        public void ShowNumberParkingSpaces(int freePlaces, int occupiedPlaces)
        {
            // Redraw screen
            ShowOptions(_options);
            WriteTextToCenter($"There is {freePlaces} free and {occupiedPlaces} occupied places");
        }


        /// <summary>
        /// Shows all current transactions till writing to log
        /// </summary>
        public void ShowCurrentTransactions(TransactionInfo[] transactionInfo)
        {
            // Redraw screen
            ShowOptions(_options);
            if (transactionInfo != null && transactionInfo.Length != 0)
            {
                int i = 1;
                foreach (TransactionInfo info in transactionInfo)
                {
                    Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 38)));
                    string line = String.Format("\t {0,-3} {1, -13} {2, -13} {3, -10} ", i++, info.TransactionTime.ToLongTimeString(), info.VehicleId, info.Sum.ToString("C"));
                    Console.WriteLine(line);
                }
                Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 38)));
                Console.WriteLine("");
            }
            else
            {
                WriteTextToCenter("There are no current transactions");
            }
        }


        /// <summary>
        /// Shows all transactions history from log
        /// </summary>
        public void ShowTransactionsHistory(string history)
        {
            // Redraw screen
            ShowOptions(_options);
            string[] transactions;

            if (string.IsNullOrEmpty(history) == false && (transactions = history.Split('\n', StringSplitOptions.RemoveEmptyEntries).Where(p => p.Length > 10).ToArray()).Length != 0)
            {
                int i = 1;

                foreach (string item in transactions)
                {
                    Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 51)));
                    Console.WriteLine($"\t{i++}. {item}");
                }
                Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 51)));
                Console.WriteLine("");
            }
            else
            {
                WriteTextToCenter("Transaction history is empty! Try later.");
            }
        }


        /// <summary>
        /// Shows list of all the vehicles on the parking service
        /// </summary>
        public void ShowVehiclesList(ReadOnlyCollection<Vehicle> vehicles)
        {
            // Redraw screen
            ShowOptions(_options);
            if (vehicles != null && vehicles.Count != 0)
            {
                int i = 1;
                foreach (Vehicle vehicle in vehicles)
                {
                    Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 45)));
                    Console.WriteLine("\t{0,-1} | {1, -10} | {2, -14} | {3, -10} |", i++, vehicle.Id, vehicle.VehicleType, vehicle.Balance.ToString("0.00", CultureInfo.InvariantCulture));
                }
                Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 45)));
                Console.WriteLine("");
            }
            else
            {
                WriteTextToCenter("There are no vehicles");
            }
        }


        /// <summary>
        /// Puts a vehicle on the Parking Service
        /// </summary>
        public Vehicle ShowPutVehicle()
        {
            // Redraw screen
            ShowOptions(_options);
            (string id, string type, string balance) = PrintTable();

            // Check entered id
            Regex regex = new Regex(Vehicle.VehicleIdPatern);
            if (id == "gen")
            {
                id = Vehicle.GenerateRandomRegistrationPlateNumber();
            }
            else if (regex.IsMatch(id) != true)
            {
                return null;
            }

            // Check entered type
            VehicleType vehicleType;
            switch (type)
            {
                case "1":
                case "PassengerCar":
                    vehicleType = VehicleType.PassengerCar;
                    break;
                case "2":
                case "Truck":
                    vehicleType = VehicleType.Truck;
                    break;
                case "3":
                case "Bus":
                    vehicleType = VehicleType.Bus;
                    break;
                case "4":
                case "Motorcycle":
                    vehicleType = VehicleType.Motorcycle;
                    break;
                default:
                    return null;
            }

            // Check entered balance
            decimal outBalance = 0;
            if (decimal.TryParse(balance, out outBalance) == true)
            {
                // if entered balance is below zero, then outBalance = 0
                outBalance = outBalance < 0 ? 0 : outBalance;
            }
            else
            {
                return null;
            }
            return new Vehicle() { Id = id, VehicleType = vehicleType, Balance = outBalance };
        }


        /// <summary>
        /// Draws a table for user input. This supporting method is required for ShowPutVehicle()
        /// </summary>
        /// <returns>Returns Tuple with the entered values</returns>
        private (string id, string type, string balance) PrintTable()
        {
            ConsoleColor defaultColor = ConsoleColor.Yellow;
            ConsoleColor selectionColor = ConsoleColor.Green;

            Console.WriteLine("");
            Console.ForegroundColor = defaultColor;
            Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 97)));
            Console.WriteLine("\t| {0, 15} => |{1, -16}| {2, -55} |", "Id".PadRight(7, ' '), " ", "'XX-YYYY-XX' or type 'gen' for auto generation");
            Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 97)));
            Console.WriteLine("\t| {0, 15} => |{1, -16}| {2, -55} |", "Type".PadRight(7, ' '), " ", "1-PassengerCar, 2-Truck, 3-Bus, 4-Motorcycle");
            Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 97)));
            Console.WriteLine("\t| {0, 15} => |{1, -16}| {2, -55} |", "Balance".PadRight(7, ' '), " ", "initial balance, the value must be greater than zero");
            Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 97)));

            (int left, int top) = Console.GetCursorPosition();
            // Get vehicle Id
            Console.SetCursorPosition(32, top - 6);
            Console.ForegroundColor = selectionColor;
            string id = Console.ReadLine();
            Console.ForegroundColor = defaultColor;

            // Get vehicle type
            Console.SetCursorPosition(32, top - 4);
            Console.ForegroundColor = selectionColor;
            string type = Console.ReadLine();
            Console.ForegroundColor = defaultColor;

            // Get vehicle balance
            Console.SetCursorPosition(32, top - 2);
            Console.ForegroundColor = selectionColor;
            string balance = Console.ReadLine();
            Console.ForegroundColor = defaultColor;

            // Set all to the default position and to the default color
            Console.SetCursorPosition(0, top);
            Console.ForegroundColor = ConsoleColor.White;

            return (id, type, balance);
        }


        /// <summary>
        /// Removes a vehicle from the Parking
        /// </summary>
        public string[] ShowPickUpVehicle(ReadOnlyCollection<Vehicle> vehicles)
        {
            // Redraw screen
            ShowOptions(_options);

            if (vehicles != null && vehicles.Count > 0)
            {
                int counter = 1;
                foreach (Vehicle vehicle in vehicles)
                {
                    Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 48)));
                    string line = String.Format("\t{0,-1} | {1, -10} | {2, -12} | {3, -10} | {4, -1} |", counter++, vehicle.Id, vehicle.VehicleType, vehicle.Balance.ToString("0.00", CultureInfo.InvariantCulture), "");
                    Console.WriteLine(line);
                }
                Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 48)));
                Console.WriteLine("");
                ColorWrite("\tMark the vehicles to remove by symbol *", ConsoleColor.Yellow);
                (int left, int top) = Console.GetCursorPosition();

                // New cursor position
                int lineAmount = 2 * (counter - 2) + 3;
                List<int> removePosition = new List<int>();

                // Draw table
                for (int i = 0; i < counter - 1; i++)
                {
                    Console.SetCursorPosition(53, top - lineAmount + 2 * i);
                    if (Console.ReadLine() == "*")
                    {
                        removePosition.Add(i);
                    }
                }
                Console.SetCursorPosition(left, top);
                return vehicles.Where((v, i) => removePosition.Contains(i)).Select(p => p.Id).ToArray();
            }
            return null;
        }


        /// <summary>
        /// Tops up a balance of a choosen vehicle
        /// </summary>
        public IEnumerable<(string id, decimal sum)> ShowTopUpVehicleBalance(ReadOnlyCollection<Vehicle> vehicles)
        {
            // Redraw screen
            ShowOptions(_options);
            if (vehicles != null && vehicles.Count > 0)
            {
                int counter = 1;
                foreach (Vehicle vehicle in vehicles)
                {
                    Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 53)));
                    string line = String.Format("\t{0,-1} | {1, -10} | {2, -12} | {3, -10} | {4, -6} |", counter++, vehicle.Id, vehicle.VehicleType, vehicle.Balance.ToString("0.00", CultureInfo.InvariantCulture), "");
                    Console.WriteLine(line);
                }
                Console.WriteLine('\t' + string.Concat(Enumerable.Repeat("-", 53)));
                Console.WriteLine("");
                ColorWrite("\tEnter sum in the cells opposite the vehicles", ConsoleColor.Yellow);
                (int left, int top) = Console.GetCursorPosition();

                // Calc new cursor position
                int lineAmount = 2 * (counter - 2) + 3;
                List<(string id, decimal sum)> list = new List<(string id, decimal sum)>();

                // Draw table
                decimal topUpSum = 0m;
                for (int i = 0; i < counter - 1; i++)
                {
                    Console.SetCursorPosition(53, top - lineAmount + 2 * i);

                    if (decimal.TryParse(Console.ReadLine(), out topUpSum) == true)
                    {
                        topUpSum = topUpSum < 0 ? 0 : topUpSum;
                        list.Add((vehicles[i].Id, topUpSum));
                    }
                }
                Console.SetCursorPosition(left, top);
                return list;
            }
            return null;
        }

        /// <summary>
        /// Gets vehicle Id
        /// </summary>
        public string ShowGetVehicleById()
        {
            // Redraw screen
            ShowOptions(_options);

            WriteTextToCenter("Enter desired vehicle Id below");
            string id = GetTextResponse().TrimStart().TrimEnd();

            Regex regex = new Regex(Vehicle.VehicleIdPatern);
            if (regex.IsMatch(id) == true)
            {
                return id;
            }
            return null;
        }
    }
}

